import { module, test } from "qunit"
import { setupTest } from "ember-qunit"

module("Unit | Route | oidccode", function(hooks) {
  setupTest(hooks)

  test("it exists", function(assert) {
    let route = this.owner.lookup("route:oidccode")
    assert.ok(route)
  })
})
