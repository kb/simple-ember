import { module, test } from "qunit"
import { setupTest } from "ember-qunit"

module("Unit | Route | taggitfilter", function(hooks) {
  setupTest(hooks)

  test("it exists", function(assert) {
    let route = this.owner.lookup("route:taggitfilter")
    assert.ok(route)
  })
})
