import { Factory } from "ember-cli-mirage"

export default Factory.extend({
  username(i) {
    return "UsernameNo" + (i + 1)
  },
  first(i) {
    return "FirstNameNo" + (i + 1)
  },
  last(i) {
    return "LastNameNo" + (i + 1)
  },
  email(i) {
    return `email_no${i}@gmaul.com`
  }
})
