import { RestSerializer } from "ember-cli-mirage"
import { dasherize, underscore } from "@ember/string"
import { pluralize } from "ember-inflector"

export default RestSerializer.extend({
  serialize(object) {
    let json = RestSerializer.prototype.serialize.apply(this, arguments)
    let result = json[object.modelName]
    return result
  },
  keyForAttribute(attr) {
    return underscore(attr)
  },
  alwaysIncludeLinkageData: true
})
