import ENV from "../config/environment"

export default function() {
  this.urlPrefix = ENV.APP.API_URL
  this.namespace = "/api/0.1"
  this.timing = 0
  this.logging = false

  this.post("http://localhost:4200/back/token/", function(request) {
    return {
      token: "access.token",
      contact_id: 2
    }
  })

  this.get("contacts", (schema, request) => {
    return schema.contacts.all().models
  })
  this.get("contacts/:id", (schema, request) => {
    return schema.contacts.find(request.params.id)
  })
}
