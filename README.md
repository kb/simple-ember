# simple-ember

Demo for a KB Software EmberJs Octane ready app.

## Usage

```
git clone git@gitlab.com:kb/simple-ember.git
cd simple-ember
npm i
ember serve
```

## Creating a new Simple Ember of your own

- In the root of the project's folder, run `ember new front` to build a new Ember App.
- `cd front`
- [Install KB Software's Ember AddOns](https://gitlab.com/kb/kb-base-ember-addons)

- Possible useful addons

```
cd front
ember install ember-cp-validations
ember install ember-cli-mirage
ember install ember-simple-auth
npm i prettier -D
npm i faker -D
npm i ember-template-lint-plugin-prettier -D
# ember install ember-moment
# ember install @ember/jquery
https://github.com/DockYard/ember-composable-helpers
https://github.com/jmurphyau/ember-truth-helpers
```

- Add the following to the scripts section of your `front/package.json` file:

```
"scripts": {
  ...
  "prettier:hbs": "prettier  --write **/*.hbs --parser=glimmer",
  "prettier": "prettier --write \"**/*.{js,json,scss,md,html}\" \"!package.json\""
}
```

### CK Editor Setup

#### Download CkEditor

- Download from <https://ckeditor.com/cke4/builder>

  1. Standard
  2. Feature SelectioN:

  3. Add Additional Plugins:

  - OnChange
  - Paste As Plain Text
  - Placeholder

  4. Remove Plugins

  - FileBrowser

  5. Moona-Lisa

  6. English + Optimized

- Extract downloaded zip and copy the `ckeditor` folder into `vendor`

#### Update the `index.html`

```html
<!-- app/index.html -->
{{content-for "body"}}

<!-- No leading slash resolves CKEDITOR "no reference" issues -->
<script type="text/javascript">
  window.CKEDITOR_BASEPATH = "{{rootURL}}assets/ckeditor/"
</script>
<!-- add -->

<script src="{{rootURL}}assets/vendor.js"></script>
<script src="{{rootURL}}assets/front.js"></script>

{{content-for "body-footer"}}
```

#### Edit the `tests/index.html` file:

```html
<!-- front/tests/index.html -->
<script type="text/javascript">
  window.CKEDITOR_BASEPATH = "{{rootURL}}assets/ckeditor/"
</script>
<!-- add -->

...

<script src="{{rootURL}}assets/front.js"></script>
<script src="{{rootURL}}assets/ckeditor/ckeditor.js"></script>
<!-- add -->
<script src="{{rootURL}}assets/tests.js"></script>
```

#### Funnel ckeditor

- Update `front/ember-cli-build.js`

```javascript
// /ember-cli-build.js
const Funnel = require("broccoli-funnel")

let app = new EmberApp(defaults, {
  // Add options here
  fingerprint: {
    exclude: ["assets/ckeditor/"]
  }
})
app.import("vendor/ckeditor/ckeditor.js")
var ckeditorAssets = new Funnel("vendor/ckeditor", {
  srcDir: "/",
  destDir: "/assets/ckeditor"
})

return app.toTree([ckeditorAssets])
```

### TailwindCss Setup

We will follow the example demonstrated here:

- [Emberjs/Tailwind/Purgecss Example](https://github.com/chrism/emberjs-tailwind-purgecss)

In this document they suggest creating a special "app/tailwind" folder, but we will just use the `app/styles` folder as a single directory to hold CSS related files.

#### Step 1: `ember-cli-build.js`

Update the `ember-cli-build.js` file.

```
'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');
const isProduction = EmberApp.env() === 'production';

const purgeCSS = {
  module: require('@fullhuman/postcss-purgecss'),
  options: {
    content: [
      // add extra paths here for components/controllers which include tailwind classes
      './app/index.html',
      './app/templates/**/*.hbs'
    ],
    defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
  }
}

module.exports = function(defaults) {
  let app = new EmberApp(defaults, {
    postcssOptions: {
      compile: {
        plugins: [
          {
            module: require('postcss-import'),
            options: {
              path: ['node_modules']
            }
          },
          require('tailwindcss')('./app/styles/tailwind.config.js'),
          ...isProduction ? [purgeCSS] : []
        ]
      }
    }
  });
  return app.toTree();
};
```

#### Step 2: `app.css`

Add the following to your project's `app/styles/app.css` file:

```
@import "tailwindcss/base";
@import "tailwindcss/components";
@import "@kbsoftware/kb-base-ember-addons/addon/styles/base-classes";
@import "@kbsoftware/kb-base-ember-addons/addon/styles/base-core";
@import "@kbsoftware/kb-base-ember-addons/addon/styles/base-detail";
@import "@kbsoftware/kb-base-ember-addons/addon/styles/base-form";
@import "@kbsoftware/kb-base-ember-addons/addon/styles/base-menu";
@import "@kbsoftware/kb-base-ember-addons/addon/styles/base-nav";
@import "@kbsoftware/kb-base-ember-addons/addon/styles/base-table";
@import "tailwindcss/utilities";
```

#### Step 3: `tailwind.config.js`

Create a file called `app/styles/tailwind.config.js` with the following content.

```
/*global module*/
/*eslint no-undef: "error"*/
/*eslint-env node*/
const { colors } = require("tailwindcss/defaultTheme")

module.exports = {
  theme: {
    fontFamily: {
      body: "Ubuntu",
      fa: "FontAwesome"
    },
    colors: {
      primary: "#1f8dd6",
      primaryBlush: "#deeefe",
      secondary: "#265778",
      grayHeavy: "#202020",
      grayDarkest: colors.gray["800"],
      grayDark: colors.gray["600"],
      grayLight: colors.gray["400"],
      grayVanishing: colors.gray["200"],
      yellow: colors.yellow["300"],
      red: colors.red["800"],
      green: colors.green["500"],
      black: colors.black,
      white: colors.white,
      transparent: colors.transparent,
      DUE: "#ecc94b",
      ACTIVE: "#48bb78",
      EXPIRING: "#ed8936",
      REQUIRED: "#f56565"
    }
  },
  corePlugins: {
    container: false
  },
  variants: {
    backgroundColor: ["responsive", "hover", "focus", "even"]
  }
}
```

Also see: [balinterdi's way](https://www.balinterdi.com/blog/ember-tailwind-css-postcss-import/))

### Useful starting files you might copy from **simple-ember** into your new Ember App

- `app/adapters/application.js`: Preps for KB's flavoured endpoints in Django Rest Framework.
- `app/initializers/main.js`: Handle deprecation message (see [deprecation-warnings](doc/deprecation-warnings.md))
- `prettier.config.js` and `.prettierignore`: Prettier settings.
