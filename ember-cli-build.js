"use strict"

const EmberApp = require("ember-cli/lib/broccoli/ember-app")
const Funnel = require("broccoli-funnel")
const isProduction = EmberApp.env() === "production"

const purgeCSS = {
  module: require("@fullhuman/postcss-purgecss"),
  options: {
    content: ["./app/index.html", "./app/templates/**/*.hbs"],
    defaultExtractor: content => content.match(/[A-Za-z0-9-_:/]+/g) || []
  }
}

module.exports = function(defaults) {
  let app = new EmberApp(defaults, {
    fingerprint: {
      exclude: ["assets/ckeditor/"]
    },
    postcssOptions: {
      compile: {
        plugins: [
          {
            module: require("postcss-import"),
            options: {
              path: ["node_modules"]
            }
          },
          require("tailwindcss")("./app/styles/tailwind.config.js"),
          ...(isProduction ? [purgeCSS] : [])
        ]
      }
    }
  })
  app.import("vendor/ckeditor/ckeditor.js")
  var ckeditorAssets = new Funnel("vendor/ckeditor", {
    srcDir: "/",
    destDir: "/assets/ckeditor"
  })
  return app.toTree([ckeditorAssets])
}
