import KbFormManager from "@kbsoftware/kb-base-ember-addons/components/kb-form-manager"
import { action } from "@ember/object"

export default class CarComponent extends KbFormManager {
  @action actionValidateThenSubmit() {
    // Tells components to show validation methods.
    this.showValidations = true
    if (this.formIsValid) {
      this.args.actionSubmitHook()
    }
  }
}
