import KbFormManager from "@kbsoftware/kb-base-ember-addons/components/kb-form-manager"
import { action } from "@ember/object"

export default class ExampleFormComponent extends KbFormManager {
  @action
  actionValidateThenSubmit() {
    this.showValidations = true
    if (this.formIsValid) {
      this.args.actionSaveHook(this.model)
    }
  }
}
