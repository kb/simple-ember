import Component from "@glimmer/component"
import { action } from "@ember/object"
import { tracked } from "@glimmer/tracking"

export default class CarComponent extends Component {
  @tracked visiState = false
  @action
  clickity() {
    this.visiState = !this.visiState
  }
  get visibility() {
    return this.visiState
      ? `block text-sm  bg-gray-400 even:bg-gray-200`
      : "hidden"
  }
  get styleWhenParent() {
    return this.hasHiderableContent ? `text-2xl cursor-pointer bg-white` : ""
  }
  get buttonIcon() {
    return this.visiState ? "up" : "down"
  }
  get hasHiderableContent() {
    if (!this.args.rowGroup.items) return false
    return this.args.rowGroup.items.length > 0
  }
}
