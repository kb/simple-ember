import Controller from "@ember/controller"
import { action } from "@ember/object"
import { inject } from "@ember/service"

export default class FormController extends Controller {
  @inject flashMessages

  daysOfWeek = [
    { abbrev: "MON", full: "Monday" },
    { abbrev: "TUE", full: "Tuesday" },
    { abbrev: "WED", full: "Wednesday" },
    { abbrev: "THU", full: "Thursday" },
    { abbrev: "FRI", full: "Friday" },
    { abbrev: "SAT", full: "Saturday" },
    { abbrev: "SUN", full: "Sunday" }
  ]

  @action
  actionSubmitSimpleForm() {
    this.flashMessages.alert(`Simple Form is valid.`)
  }
}
