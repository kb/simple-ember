import Controller from "@ember/controller"
import { action } from "@ember/object"
import { inject } from "@ember/service"

export default class ApplicationController extends Controller {
  @inject flashMessages
  @inject session
  @action
  logout() {
    this.session.invalidate()
  }
}
