import Controller from "@ember/controller"
import { action } from "@ember/object"
import { inject } from "@ember/service"

export default class UploadController extends Controller {
  @inject flashMessages

  @action
  submitForm() {
    this.flashMessages.alert("We will now process your files.")
    this.model.document.fileList.forEach(file => {
      this.flashMessages.alert(`${file} processed!`)
    })
  }
}
