import Controller from "@ember/controller"
import { tracked } from "@glimmer/tracking"

export default class TaggitFilterController extends Controller {
  @tracked tags = ["red", "green", "blue"]
}
