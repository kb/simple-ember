import DataAdapterMixin from "ember-simple-auth/mixins/data-adapter-mixin"
import DRFAdapter from "./drf"
import ENV from "../config/environment"
import { computed } from "@ember/object"
import { inject as service } from "@ember/service"
import { isEmpty } from "@ember/utils"

export default DRFAdapter.extend(DataAdapterMixin, {
  headers: computed("session.data.authenticated", function() {
    const headers = {}
    /* eslint no-unused-vars: 0 */
    let { notUsed, token } = this.get("session.data.authenticated")
    if (token && !isEmpty(token)) {
      headers["Authorization"] = "Token " + token
    }
    return headers
  }),
  host: ENV.APP.API_URL,
  namespace: "api/0.1",
  session: service("session"),
  shouldReloadRecord(store, snapshot) {
    return false
  },
  shouldReloadAll(store, snapshot) {
    return false
  },
  shouldBackgroundReloadRecord(store, snapshot) {
    return true
  },
  shouldBackgroundReloadAll(store, snapshot) {
    return true
  }
})
