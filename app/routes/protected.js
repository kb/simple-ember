import AuthenticatedRouteMixin from "ember-simple-auth/mixins/authenticated-route-mixin"
import KbModelRoute from "@kbsoftware/kb-base-ember-addons/routes/kb-model-route"

export default class ProtectedRoute extends KbModelRoute.extend(
  AuthenticatedRouteMixin
) {
  model() {
    return { loggedInContact: this.fetchCurrentContact.perform() }
  }
}
