import Route from "@ember/routing/route"

export default class PaginatorRoute extends Route {
  model() {
    return [
      { make: "citroen", model: "xantia sx 1905cc", shape: "hatchback" },
      { make: "ford", model: "cougar 16v 2.0 3dr", shape: "sportscoupe" },
      { make: "ford", model: "fiesta lx 1.25", shape: "hatchback" },
      { make: "ford", model: "fiesta zet 1.4 3dr", shape: "hatchback" },
      { make: "ford", model: "fiesta zetec ghia 1388cc", shape: "hatchback" },
      { make: "ford", model: "focus ghia 2.0 4dr", shape: "saloon" },
      { make: "ford", model: "mondeo gh.x 2.0td", shape: "saloon" },
      { make: "ford", model: "puma rac 1.7 3dr", shape: "sportscoupe" },
      { make: "vauxhall", model: "astra ls", shape: "hatchback" },
      { make: "vauxhall", model: "corsa gls 1.2 5dr", shape: "hatchback" },
      { make: "vauxhall", model: "omega gls 1998cc", shape: "saloon" },
      { make: "vauxhall", model: "tigra 1.6 coupe", shape: "sportscoupe" },
      { make: "vauxhall", model: "vectra envoy 1598", shape: "hatchback" }
    ]
  }
}
