import Route from "@ember/routing/route"

export default class PaginatorRoute extends Route {
  model() {
    return [
		{ "job": "Doctor", "salary": "£200,000", "positions": "3" },
		{ "job": "School Counselor", "salary": "£20,000", "positions": "2" },
		{ "job": "Lead Hazard Reduction", "salary": "£100,000", "positions": "3" },
		{ "job": "Naturopath", "salary": "£2,000", "positions": "4" },
		{ "job": "Lottery Agent", "salary": "£30,000", "positions": "5" },
		{ "job": "Embalmer", "salary": "£10,000", "positions": "1" },
		{ "job": "Real Estate Appraiser", "salary": "£50,000", "positions": "0" },
		{ "job": "Athlete", "salary": "£100,000", "positions": "3" },
		{ "job": "Travel Agent", "salary": "£40,000", "positions": "2" },
		{ "job": "Family Group Day Care Home Provider", "salary": "£20,000", "positions": "4" },
		{ "job": "Flight Attendant", "salary": "£30,000", "positions": "2" },
		{ "job": "Wildlife Propagator", "salary": "£60,000", "positions": "1" },
		{ "job": "Geneticist", "salary": "£10,000", "positions": "5" },
		{ "job": "Fashion Designer", "salary": "£40,000", "positions": "3" },
		{ "job": "City Planner", "salary": "£50,000", "positions": "3" },
		{ "job": "Physician Assistant", "salary": "£30,000", "positions": "3" },
		{ "job": "Botanist", "salary": "£40,000", "positions": "3" },
		{ "job": "Fur Buyer", "salary": "£70,000", "positions": "3" },
		{ "job": "Marriage and Family Therapist", "salary": "£20,000", "positions": "5" },
		{ "job": "Manicurist", "salary": "£40,000", "positions": "3" },
		{ "job": "Clinical Laboratory Technician", "salary": "£10,000", "positions": "2" },
		{ "job": "Funeral Director", "salary": "£60,000", "positions": "3" },
		{ "job": "Drinking Water Treatment and Distribution Operator", "salary": "£80,000", "positions": "3" },
		{ "job": "School Bus Driver", "salary": "£30,000", "positions": "3" },
		{ "job": "IT Training", "salary": "£20,000", "positions": "3" },
		{ "job": "Director", "salary": "£50,000", "positions": "3" },
		{ "job": "Telecommunications System Technician", "salary": "£70,000", "positions": "3" },
		{ "job": "Foster Parent", "salary": "£30,000", "positions": "3" },
		{ "job": "Optometrist", "salary": "£40,000", "positions": "3" },
		{ "job": "Investment Advisor Representative", "salary": "£60,000", "positions": "3" },
		{ "job": "EngineerEnvironmental Lead Inspector", "salary": "£60,000", "positions": "3" },
		{ "job": "Nuclear Medicine Technologist", "salary": "£20,000", "positions": "3" },
		{ "job": "Sales and Marketing Manager", "salary": "£10,000", "positions": "3" },
		{ "job": "School Nurse-Teacher", "salary": "£60,000", "positions": "3" },
		{ "job": "Physical Therapist Assistant", "salary": "£30,000", "positions": "3" },
		{ "job": "Pilot", "salary": "£60,000", "positions": "3" },
		{ "job": "Choreographers", "salary": "£30,000", "positions": "3" },
		{ "job": "Historian", "salary": "£50,000", "positions": "3" },
		{ "job": "Biologist", "salary": "£70,000", "positions": "3" },
		{ "job": "Insurance Producer", "salary": "£70,000", "positions": "3" },
		{ "job": "Photographer", "salary": "£60,000", "positions": "3" },
		{ "job": "Purchasing Manager", "salary": "£50,000", "positions": "3" },
		{ "job": "Journalist", "salary": "£40,000", "positions": "3" },
		{ "job": "Acupuncturist", "salary": "£20,000", "positions": "3" },
		{ "job": "Survey Technicians and Technologists", "salary": "£70,000", "positions": "3" },
		{ "job": "Insurance Claim Adjuster", "salary": "£20,000", "positions": "3" },
		{ "job": "Hoisting Engineer", "salary": "£40,000", "positions": "3" },
		{ "job": "Ratcatcher", "salary": "£60,000", "positions": "3" },
		{ "job": "Charity Worker", "salary": "£7,000", "positions": "3" }
    ]
  }
}
