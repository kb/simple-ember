import Route from "@ember/routing/route"

export default class FormRoute extends Route.extend() {
  model() {
    return {
      firstName: "",
      lastName: "",
      dob: "",
      email: "",
      day: "",
      termsAccepted: false
    }
  }
}
