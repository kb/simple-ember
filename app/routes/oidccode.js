import Route from "@ember/routing/route"

export default class OidccodeRoute extends Route {
  /** This mocks the host OIDC endpoint, e.g. Microsoft Online. Normally a user
   * and pass would be required by the host before it generates a code and
   * redirects to the redirect uri. This route mocks that call without asking
   * for credentials.
   */
  redirect(_, transition) {
    const { redirect_uri, state } = transition.to
      ? transition.to.queryParams
      : transition.queryParams
    window.location.replace(`${redirect_uri}?code=123456789&state=${state}`)
  }
}
