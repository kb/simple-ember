import OidcAuthenticationRouteMixin from "@kbsoftware/kb-base-ember-addons/mixins/oidc-authentication-route"
import Route from "@ember/routing/route"

export default class LoginRoute extends Route.extend(
  OidcAuthenticationRouteMixin
) {}
