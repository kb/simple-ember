import Route from "@ember/routing/route"

export default class TaggitFilterRoute extends Route {
  model() {
    return [
      { color: "Red", hex: "#FF0000", tags: ["red"] },
      { color: "Yellow", hex: "#FFFF00", tags: ["red", "green"] },
      { color: "Fuchsia", hex: "#FF00FF", tags: ["red", "blue"] },
      { color: "Magenta", hex: "#FF00FF", tags: ["red", "blue"] },
      { color: "Lime", hex: "#00FF00", tags: ["green"] },
      { color: "Aqua", hex: "#00FFFF", tags: ["green", "blue"] },
      { color: "Cyan", hex: "#00FFFF", tags: ["green", "blue"] },
      { color: "Blue", hex: "#0000FF", tags: ["blue"] },
      { color: "White", hex: "#FFFFFF", tags: ["red", "green", "blue"] },
      { color: "Black", hex: "#000000", tags: [] }
    ]
  }
}
