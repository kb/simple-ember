import ApplicationRouteMixin from "ember-simple-auth/mixins/application-route-mixin"
import Ember from "ember"
import Route from "@ember/routing/route"
import { action } from "@ember/object"
import { later } from "@ember/runloop"
import { task } from "ember-concurrency"

export default class ApplicationRoute extends Route.extend(
  ApplicationRouteMixin
) {
  routeAfterAuthentication = "protected"
  routeIfAlreadyAuthenticated = "protected"

  sessionAuthenticated() {
    super.sessionAuthenticated(...arguments)
    this.fetchCurrentContact.perform()
  }

  @action
  actionRefreshApplicationRoute() {
    this.refresh()
  }

  setupController(controller, model) {
    super.init(controller, model)
    this.flashMessages.alert("Application restarted")
    this.startPinging.perform()
  }

  @(task(function*() {
    let self = this
    // `later` creates timeout issues in tests.
    if (!Ember.testing) {
      later(() => {
        self.flashMessages.alert("Incoming vessels on the starboard bow.")
        self.startPinging.perform()
      }, 60000)
    }
    yield true
  }).restartable())
  startPinging
}
