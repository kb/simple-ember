/*global module*/
/*eslint no-undef: "error"*/
/*eslint-env node*/
// const { colors } = require("tailwindcss/defaultTheme")

module.exports = {
  theme: {
    fontFamily: {
      body: "Inter var",
      fa: "FontAwesome"
    }
    /**
    colors: {
      primary: "#1f8dd6",
      primaryBlush: "#deeefe",
      secondary: "#265778",
      grayHeavy: "#202020",
      grayDarkest: colors.gray["800"],
      grayDark: colors.gray["600"],
      grayLight: colors.gray["400"],
      grayVanishing: colors.gray["200"],
      yellow: colors.yellow["300"],
      red: colors.red["800"],
      green: colors.green["500"],
      black: colors.black,
      white: colors.white,
      transparent: colors.transparent
    }
    */
  },
  corePlugins: {
    container: false
  },
  plugins: [
    require('@tailwindcss/custom-forms'),
  ],
  variants: {
    backgroundColor: ["responsive", "hover", "focus", "even"]
  }
}
