import EmberRouter from "@ember/routing/router"
import config from "./config/environment"

export default class Router extends EmberRouter {
  location = config.locationType
  rootURL = config.rootURL
}

Router.map(function() {
  this.route("home", { path: "" })
  this.route("form")
  this.route("ckeditor")
  this.route("upload")
  this.route("paginator")
  this.route("groupby")
  this.route("dash")
  this.route("taggitfilter")
  this.route("login")
  this.route("protected")
  this.route("oidccode")
})
