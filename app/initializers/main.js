import Ember from "ember"
import { registerDeprecationHandler } from "@ember/debug"

export function initialize() {
  if (Ember.Debug && typeof registerDeprecationHandler === "function") {
    registerDeprecationHandler((message, options, next) => {
      if (!(options && options.until && options.until !== "2.0.0")) {
        next(message, options)
      }
    })
  }
}

export default { initialize }
