import Model, { attr } from "@ember-data/model"

export default class ContactModel extends Model {
  @attr username
  @attr first
  @attr last
  @attr email
  get fullName() {
    return `${this.first} ${this.last}`
  }
  get nameAbbrev() {
    return this.first[0]
  }
}
