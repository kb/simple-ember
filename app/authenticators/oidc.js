import Base from "ember-simple-auth/authenticators/base"
import { Promise } from "rsvp"
import { inject } from "@ember/service"
import { isEmpty } from "@ember/utils"
import { run } from "@ember/runloop"
import composeFetchCall from "@kbsoftware/kb-base-ember-addons/utils/kb-fetch-call"
import config from "@kbsoftware/kb-base-ember-addons/config"
import environmentConfig from "ember-get-config"
import fetch from "fetch"

const { tokenEndPoint } = config
const { environment } = environmentConfig

export default Base.extend({
  cookies: inject(),
  authenticate(code) {
    return new Promise((resolve, reject) => {
      let csrfToken = this.cookies.read("csrftoken")
      fetch(
        `${tokenEndPoint}`,
        composeFetchCall(
          "post",
          { code: code },
          csrfToken,
          environment === "production"
        )
      )
        .then(response => response.json())
        .then(json => {
          run(() => {
            resolve({ contact_id: json.contact_id, token: json.token })
          })
        })
        .catch(ex => {
          reject(ex)
        })
    })
  },
  restore(data) {
    return new Promise((resolve, reject) => {
      if (!isEmpty(data.token)) {
        resolve(data)
      } else {
        reject()
      }
    })
  }
})
