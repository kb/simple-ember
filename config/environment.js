"use strict"

module.exports = function(environment) {
  let ENV = {
    modulePrefix: "simple-ember",
    environment,
    rootURL: "/",
    locationType: "auto",
    "ember-simple-auth": {
      authenticationRoute: "login",
      routeAfterAuthentication: "home",
      routeIfAlreadyAuthenticated: "home"
    },
    oidcAuth: {
      afterLogoutUri: "http://localhost:4200",
      authEndPoint: "/oidccode/",
      clientId: "client_id",
      host: "http://localhost:4200",
      tokenEndPoint: "http://localhost:4200/back/token/"
    },

    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. EMBER_NATIVE_DECORATOR_SUPPORT: true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },
    flashMessageDefaults: {
      // flash message defaults
      timeout: 3000,
      extendedTimeout: 1000,
      preventDuplicates: true,
      priority: 2000
    },
    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  }

  if (environment === "development") {
    ENV.APP.API_URL = "http://localhost:8000/back"
    ENV["ember-cli-mirage"] = {
      enabled: true
    }
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === "test") {
    ENV.APP.API_URL = "http://localhost:8080"
    ENV["ember-cli-mirage"] = {
      enabled: true
    }
    // Testem prefers this...
    ENV.locationType = "none"
    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false
    ENV.APP.LOG_VIEW_LOOKUPS = false

    ENV.APP.rootElement = "#ember-testing"
    ENV.APP.autoboot = false
  }

  if (environment === "production") {
    // here you can enable a production-specific feature
  }

  return ENV
}
