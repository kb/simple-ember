# Simple Ember Demo API

Use this when testing the Kb Upload component.

## Install

```
cd simple-ember/api
virtualenv --python=python3 venv
source venv/bin/activate
# or
source venv/bin/activate.fish
pip install flask flask-cors
python main.py
```

## Usage

```
cd simple-ember/api
source venv/bin/activate
# or
source venv/bin/activate.fish
python main.py
```

## Uninstall

```
cd simple-ember/api
rm -rf venv
```
