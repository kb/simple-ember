# django-cors-headers

## Issue

```
Access to XMLHttpRequest at 'http://localhost:8000/api/0.1/tasks/' from origin 'http://localhost:4200' has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource.
```

## Solution

- [cors-in-django-rest](https://dev.to/techiediaries/handling-cors-in-django-rest-framework-1177)

For development install `django-cors-headers` in your development machine. Add this to `requirements/local.txt`.

Then add this to local settings:

```
CORS_ORIGIN_ALLOW_ALL = True
CORS_ALLOW_CREDENTIALS = True
CORS_ORIGIN_WHITELIST = ("http://localhost:8000",)

INSTALLED_APPS += ("corsheaders",)
MIDDLEWARE = ("corsheaders.middleware.CorsMiddleware",) + MIDDLEWARE
```
