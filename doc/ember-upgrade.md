# Ember Upgrade

Is odd.

During the process ember will put your code onto an orphaned branch. You will lose .private, your venv folder, and other files ignored by git.

The process is as follows:

```
cd front
# get latest ember-cli-update
yarn upgrade ember-cli-update
# run it to update and resolve merge conflicts. You may have to merge some manually.
ember update --resolve-conflicts
# run it and resolve any issues
ember update --run-codemods
# run it to update all add ons.
yarn upgrade-interactive --latest
```
