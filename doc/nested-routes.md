# Nested Routes

In the message route, I added a nested route for conversation. This is standard ember.

## Generate

```bash
ember g route message/conversation
```

## The router

```javascript
// front/app/router.js
this.route("message", function() {
  this.route("conversation", { path: "/:conversation_id" })
})
```

## The parent route

```javascript
// front/app/routes/message.js
export default Route.extend(AuthenticatedRouteMixin, {
  model() {
    return this.store.findAll("conversation")
  }
})
```

## The child route

```javascript
// front/app/routes/message/conversation.js
export default Route.extend(AuthenticatedRouteMixin, {
  model(params) {
    return this.store.query("message", {
      conversation_id: params.conversation_id
    })
  }
})
```

## The parent template.

The nested route template will be displayed where {{outlet}}:

```html
<!-- front/app/templates/message.hbs -->
<div>
  {{#each model as |conversation|}} {{#link-to "message.conversation"
  conversation.id}} {{ conversation.detail }} {{/link-to}}<br />
  {{/each}}
</div>
<div>
  {{outlet}}
</div>
```

REALLY IMPORTANT: You must use `conversation.id` as the parameter of `{{link-to}}`, not simply `conversation`. For the latter, the URL changes, but the nested data does not appear (until you F5).

## The child template.

```html
<!-- front/app/templates/message/conversation.hbs -->
{{#each model as |message|}} {{ message.detail }} {{/each}}
```
