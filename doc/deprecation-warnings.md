# Deprecations Warnings

- <https://guides.emberjs.com/release/configuring-ember/handling-deprecations/>

Added the following files as per link above:

- `front/app/initializers/main.js` Uncomment to kill all Deprecation Warnings, just because of how annoying they are.

- `front/config/deprecation-workflow.js` for when it's time to deal with them:

  - Already installed `ember-cli-deprecation-workflow`

  - Comment out `front/app/initializers/main.js`

  - Run the site, collecting warnings.

  - Type `deprecationWorkflow.flushDeprecations()`

  - Copy output into `front/config/deprecation-workflow.js`

  - Change the handler from `silence` to `throw` to deal with the messages one at a time.

## Related links

- <https://guides.emberjs.com/release/ember-inspector/deprecations/> per link above
