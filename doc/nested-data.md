# Readonly Nested Data

- <https://thejsguy.com/2016/01/29/working-with-nested-data-in-ember-data-models.html>
- <https://www.reddit.com/r/django/comments/6yrh9k/drf_serialization_not_working_on_many_to_many/>

## Possible use of prefetch

- <https://medium.com/quant-five/speed-up-django-nested-foreign-key-serializers-w-prefetch-related-ae7981719d3f>
