# broccoli-asset-rev

Doesn't work with variables in the src URL, e.g.

```
<img src="/icon/{{iconName}}.png">
```

broccoli-asset-rev renames image files, but only renames the associated hard coded img src's.

Solution: remove broccoli-asset-rev
