# Forgetful Things

Using the `link-to` helper for changing the query_params:

```
{{#link-to "model-detail-route" uid=model.id}}
  {{model.name}}
{{/link-to}}

<LinkTo @route="model-detail-route" @model={{model.id}}>
  {{model.name}}
</LinkTo>
```

Notice that we use the id not the model (as is often documented). This seems to kick Ember into querying the server rather than the local data for a record.

Using `id` as the parament is a better way to force model reload!
